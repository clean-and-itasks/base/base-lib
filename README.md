# base-lib

This library provides basic functionality concerning basic types and classes
defined in [StdEnv](https://gitlab.com/clean-and-itasks/base/stdenv). Also
generic classes corresponding to classes defined in
[StdEnv](https://gitlab.com/clean-and-itasks/base/stdenv) are provided.

`lib` is released in the `base-lib` package.
This package should normally not be used directly; instead, you should use `base`.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
