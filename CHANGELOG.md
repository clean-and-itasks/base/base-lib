# Changelog

### 2.12.0

- Feature: add `RECORD` derivations for `Control.GenMap` and `Control.GenMapSt`.

### 2.11.0

- Feature: add `Permutations` to `Data.List`, a version of `permutations` which returns the permutations
           of an overloaded list.

### 2.10.0

- Feature: add `toString` instance for `Either`.

#### 2.9.1

- Chore: support compiler version `4`.

### 2.9.0

- Feature: add `AnySt` to `Data.List`.

#### 2.8.1

- Fix: add missing import in definition module of Data.Either.

### 2.8.0

- Feature: add `<` instance for `:: Either`.

### 2.7.0

- Feature: add `Data.Generics` from clean platform v0.3.44.
- Feature: add `OBJECT` and `FIELD` constructors to `Data.Generics`.

### 2.6.0

- Feature: add `gLexOrd` for `:: Either`.

### 2.5.0

- Feature: add overloaded variants of the list functions `DeleteBy`, `Lookup`, `Union` and `UnionBy`.

#### 2.4.1

- Fix: `gLexOrd` derivation of `?`, `?^` and `?#` is now specialized such that it has the same behavior as `<`.

### 2.4.0

- Feature: add `==` instance for `:: Either`.

#### 2.3.2

- Fix: `<` instance for `?a`.

#### 2.3.1

- Fix: define `?None` as `gDefault` values for `?`, `?^` and `?#`.

### 2.3.0

- Feature: Remove uniqueness constraint on function that is applied unsafely by `accUnsafe` and `appUnsafe`.
- Feature: Add `<` instance for `?a`.

### 2.2.0

- Feature: `Data.List`: add `RemoveDupBy` for overloaded lists.
- Feature: `Data.List`: add `MaxListBy` for overloaded lists.
- Feature: `Data.List`: add `MinListBy` for overloaded lists.

### 2.1.0

- Feature: Add `System._Finalized` from `clean-platform`.

## 2.0.0

- Feature: Add `Either` type.
- Removed: Remove `MaybeT` (this is now in `category-theory` with the other transformers).

## 1.0.0

- Initial version, import modules from clean platform v0.3.34
- Add `concatArr`, `concatArr3`, `concatArr4`, `concatArr5`
- Add derives for maybe types
- Revert "add memcpyPointerToString/memcpyPointerToStringSt" as this introduced a dependency on c-code.
- Remove and/or rename functions from Data.List that exist in StdList under a different name:

	| Data.List         | StdList  |
	| ----------        | -------  |
	| head              | hd       |
	| tail              | tl       |
	| product           | prod     |
	| isnull            | isEmpty  |
	| elem              | isMember |
	| scanl             | scan     |
	| notElem→notMember | n.a.     |
