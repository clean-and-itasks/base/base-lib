definition module Control.GenBimap

import StdGeneric, _SystemStrictMaybes, _SystemStrictLists

derive bimap ?, ?^, ?#, [], [!], [ !], [!!], [#], [#!], (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
