definition module Control.GenMap

import StdGeneric, _SystemStrictMaybes, _SystemStrictLists

generic gMap a b :: .a -> .b
derive gMap c, UNIT, PAIR, EITHER, CONS, FIELD, OBJECT, RECORD, {}, {!}

derive gMap ?, ?^, ?#, [], [!], [ !], [!!], [#], [#!], (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
