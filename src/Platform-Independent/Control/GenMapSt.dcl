definition module Control.GenMapSt

import StdGeneric, _SystemStrictMaybes, _SystemStrictLists

generic gMapLSt a b :: .a .st -> (.b, .st)
derive gMapLSt c, UNIT, PAIR, EITHER, FIELD, CONS, OBJECT, RECORD, {}, {!}
derive gMapLSt ?, ?^, ?#, [], [!], [ !], [!!], [#], [#!], (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)

generic gMapRSt a b :: .a .st -> (.b, .st)
derive gMapRSt c, UNIT, PAIR, EITHER, FIELD, CONS, OBJECT, RECORD, {}, {!}
derive gMapRSt ?, ?^, ?#, [], [!], [ !], [!!], [#], [#!], (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
