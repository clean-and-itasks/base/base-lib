definition module Debug.StackTrace

/**
 * This module provides functions to adjust the stack trace functionality.
 * NB: this module can only be imported while stack tracing is enabled.
 * When using Nitrile see https://clean-and-itasks.gitlab.io/nitrile/nitrile-yml/reference/#clm_options for how to
 * enable stack tracing.
 */

/**
 * Sets a custom stack trace depth, when a program crashes, the provided number of stack frames are written to stderr.
 * Typical usage:
 *    #! world = setStackTraceDepth 42 world
 * Use strictness to ensure the function is evaluated.
 *
 * @param The number of stack frames that should be printed in the stack trace.
 * @param An environment.
 * @result The environment
 */
setStackTraceDepth :: !Int !.env -> .env
