implementation module Debug.StackTrace

import StdEnv

setStackTraceDepth :: !Int !.env -> .env;
setStackTraceDepth n env =
    IF_INT_64_OR_32
        (setStackTraceDepth64 n env)
        (setStackTraceDepth32 n env)
where
    setStackTraceDepth64 :: !Int !.env -> .env;
    setStackTraceDepth64 n env =
        code {
            pushI -8
            pushLc stack_trace_depth
            addI
            :xxx
            push_b_a 0
            pop_b 1
            fill1_r _ 0 1 0 01
            .keep 0 1
            pop_a 1
        }

    setStackTraceDepth32 :: !Int !.env -> .env;
    setStackTraceDepth32 n env =
        code {
            pushI -4
            pushLc stack_trace_depth
            addI
            :yyy
            push_b_a 0
            pop_b 1
            fill1_r _ 0 1 0 01
            .keep 0 1
            pop_a 1
        }
