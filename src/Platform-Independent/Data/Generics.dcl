definition module Data.Generics

from StdOverloaded import class toString
import StdGeneric

/**
 * Retrieves the name for the generic descriptor, i.e. the *_name field.
 * @var generic descriptor.
 * @param generic descriptor value.
 * @result name of the generic descriptor.
 */
class genericDescriptorName a :: !a -> String
instance genericDescriptorName GenericTypeDefDescriptor, GenericConsDescriptor, GenericRecordDescriptor, GenericFieldDescriptor

/**
 * Retrieves the type for the generic descriptor. For fields this is the type of the field, for a constructor this is
 * the type of the constructor function, for records and objects this is the type of the thing itself.
 *
 * @var generic descriptor
 * @param generic descriptor value.
 * @result type of the generic descriptore.
 */
class genericDescriptorType a :: !a -> GenType
instance genericDescriptorType GenericTypeDefDescriptor, GenericConsDescriptor, GenericRecordDescriptor, GenericFieldDescriptor

instance toString GenType

/**
 * Efficiently print the GenType
 *
 * @param string representation for the type variables. There is no need to go beyond 32 because the compiler cannot
 * 	handle types with more than 32 type variables. However, you need to provide at least one element for every type
 * 	variable.
 * @param the type to print
 * @param string accumulator
 * @result printed version of the provided type prepended to the accumulator
 */
printGenType :: ![String] !GenType ![String] -> [String]

//* Extract the value from an {{OBJECT}}.
fromOBJECT :: !(OBJECT x) -> x

//* Extract the value from a {{CONS}}.
fromCONS   :: !(CONS x)   -> x

//* Extract the value from a {{RECORD}}.
fromRECORD :: !(RECORD x) -> x

//* Extract the value from a {{FIELD}}.
fromFIELD  :: !(FIELD x)  -> x

//* Extract the left value from a {{PAIR}}.
fromPAIRX  :: !(PAIR x y) -> x

//* Extract the right value from a {{PAIR}}.
fromPAIRY  :: !(PAIR x y) -> y

//* Construct an {{OBJECT}} value, ({{OBJECT}} is a newtype so you can't use the constructor directly).
object :: !x -> OBJECT x

//* Construct an {{FIELD}} value, ({{FIELD}} is a newtype so you can't use the constructor directly).
field :: !x -> FIELD x
