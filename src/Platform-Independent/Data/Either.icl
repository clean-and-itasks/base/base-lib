implementation module Data.Either

import StdEnv
import Data.GenDefault
import Data.GenEq
import Data.GenFDomain
import Data.GenLexOrd

either :: .(.a -> .c) .(.b -> .c) !(Either .a .b) -> .c
either f _ (Left x)  =  f x
either _ g (Right y) =  g y

lefts :: !.[Either .a .b] -> .[.a]
lefts l = [l\\(Left l)<-l]

rights :: !.[Either .a .b] -> .[.b]
rights l = [l\\(Right l)<-l]

fromLeft :: .a !(Either .a .b) -> .a
fromLeft a e = either id (const a) e

fromRight :: .b !(Either .a .b) -> .b
fromRight a e = either (const a) id e

instance == (Either a b) | == a & == b where
	(==) (Left x)  (Left y)  = x == y
	(==) (Right x) (Right y) = x == y
	(==) _         _         = False

instance < (Either a b) | < a & < b where
	(<) (Left x)  (Left y)  = x < y
	(<) (Right x) (Right y) = x < y
	(<) (Left _)  (Right _) = True
	(<) (Right _) (Left _)  = False

instance toString (Either a b) | toString a & toString b where
	toString (Left l) = toString l
	toString (Right r) = toString r

derive gEq Either
derive gDefault Either
derive gFDomain Either
derive gLexOrd Either

derive bimap []
