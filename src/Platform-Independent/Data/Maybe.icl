implementation module Data.Maybe

import StdBool
import StdFunctions
import StdMaybe
import StdMisc
import Data.Func

// We cannot take the fromJust from StdMaybe because that version is not total.
fromJust m :== case m of ?|Just x -> x; _ -> abort "fromJust: Nothing\n"

maybe :: .b .(u:a -> .b) v:(m u:a) -> .b | Maybe m a, [v <= u]
maybe x _ ?|None     = x
maybe _ f (?|Just x) = f x

maybeSt :: *st (u:a *st -> *st) !v:(m u:a) -> *st | Maybe m a, [v<=u]
maybeSt st _ ?|None     = st
maybeSt st f (?|Just x) = f x st

fromMaybe :: u:a !(v:m u:a) -> u:a | Maybe m a, [v<=u]
fromMaybe x mb = maybe x id mb

instance < (?a) | < a where
	(<) ?None      (?Just _)  = True
	(<) (?Just a1) (?Just a2) = a1 < a2
	(<) _          _          = False
