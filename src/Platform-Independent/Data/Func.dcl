definition module Data.Func

from _SystemStrictLists import class List

/**
 * Function application.
 * @type !(a -> b) a -> b
 */
($) infixr 0
($) f :== f

/**
 * Function application, applying the second argument to the first.
 * @type a !(a -> b) -> b
 */
($&) infixl 1
($&) f x :== x f

/**
 * If function
 * @type Bool a a -> a
 */
if` c t e :== if c t e

/**
 * Function application.
 * @type a -> a
 */
app f :== f

/**
 * Apply a state function to a list of values.
 * See also {{`mapSt`}}.
 *
 * @param The function.
 * @param The list of values.
 * @param The initial state.
 * @result The final state.
 */
seqSt        :: !(a .st -> .st)       ![a] !.st -> .st

/**
 * Apply a state function to a list of values and return the results.
 * See also {{`seqSt`}}.
 *
 * @param The function.
 * @param The list of values.
 * @param The initial state.
 * @result The value results and the final state.
 */
mapSt        :: !(a .st -> (b,.st)) ![a] !.st -> (![b],.st)

/**
 * `mapSt for overloaded lists, see {{`mapSt}}.
 */
MapSt :: !(a .st -> (b, .st)) !(l a) !.st -> (!(l b), .st) | List l a & List l b
	special
		l = []; l = [!]; l = [!!]; l = [ !];

/**
 * The fixed point combinator, reducing `fix f` to `f (fix f)`.
 */
fix          :: !(a -> a) -> a

/**
 * Apply a binary function on another domain.
 *
 * Typical usage: `sortBy (on (<) toInt) :: [a] -> [a] | toInt a`
 * Or infix: `sortBy ((<) `on` toInt) :: [a] -> [a] | toInt a`
 *
 * @type (b b -> c) (a -> b) -> (a a -> c)
 */
on f g :== \x y -> f (g x) (g y)

/**
 * Infix version of {{`on`}}.
 * @type (b b -> c) (a -> b) -> (a a -> c)
 */
(`on`) infixl 0
(`on`) :== on

/**
 * Completely evaluate an expression (not just to head normal form like strictness).
 */
hyperstrict  :: !.a -> .a
