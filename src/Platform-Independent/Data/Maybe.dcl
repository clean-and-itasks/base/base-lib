definition module Data.Maybe

/**
 * This module provides functions to deal with the builtin `?` type.
 */

import _SystemStrictMaybes
from StdClass import class <
from StdMaybe import
	isJust, isNothing, isNone, isJustU, isNothingU, isNoneU,
	mapMaybe,
	instance == (?x),
	instance == (?^x),
	instance == (?#x),
	maybeToList, listToMaybe, catMaybes
from StdMisc import abort
from StdOverloaded import class ==

fromJust m :== case m of ?|Just x -> x; _ -> abort "fromJust: Nothing\n"

/**
 * Apply a function to the the contents of a Just value and directly return
 * the result, or return a default value if the argument is a Nothing value.
 */
maybe :: .b .(u:a -> .b) v:(m u:a) -> .b | Maybe m a, [v <= u]
	special m= ?; m= ?^

/**
 * Apply a function to the the contents of a Just value and the state, and
 * directly return the result and a new state. Return the state immediately
 * if the argument is a Nothing value.
 */
maybeSt :: *st (u:a *st -> *st) !v:(m u:a) -> *st | Maybe m a, [v<=u]
	special m= ?; m= ?^

/**
 * Directly return a Just value or return a default value if the argument is a Nothing value.
 */
fromMaybe :: u:a !(v:m u:a) -> u:a | Maybe m a, [v<=u]
	special m= ?; m= ?^

instance < (?a) | < a
