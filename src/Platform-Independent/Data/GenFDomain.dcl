definition module Data.GenFDomain

import StdGeneric, _SystemStrictMaybes

// gFDomain generates *all* values of a finite domain
generic gFDomain a :: [a]
derive gFDomain Bool, Char, UNIT, PAIR, EITHER, CONS, FIELD, OBJECT

derive gFDomain (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,), (?), (?^), (?#)
