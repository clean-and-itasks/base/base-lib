definition module Data.Bool

from StdOverloaded import class == (==)

/**
 * Logical implication.
 *
 * @type Bool Bool -> Bool
 */
(-->) infixr 1
(-->) p q :== if p q True

/**
 * Logical equivalence.
 *
 * @type Bool Bool -> Bool
 */
(<-->) infixr 1
(<-->) p q :== p == q