implementation module Data.Tuple

import StdEnv

import Data.Array
import Data.Maybe

tuple  :: .a .b -> .(.a,.b)
tuple a b = (a,b)

tuple3 :: .a .b .c -> .(.a,.b,.c)
tuple3 a b c = (a,b,c)

appFst:: .(.a -> .c) !(.a,.b) -> (.c,.b)
appFst f (a,b) = (f a,b)

appSnd :: .(.b -> .c) !(.a,.b) -> (.a,.c)
appSnd f (a,b) = (a,f b)

appFst3 :: .(.a -> .d) !(.a,.b,.c) -> (.d,.b,.c)
appFst3 f (a,b,c) = (f a,b,c)

appSnd3 :: .(.b -> .d) !(.a,.b,.c) -> (.a,.d,.c)
appSnd3 f (a,b,c) = (a,f b,c)

appThd3 :: .(.c -> .d) !(.a,.b,.c) -> (.a,.b,.d)
appThd3 f (a,b,c) = (a,b,f c)

swap :: !.(.a, .b) -> .(.b, .a)
swap (a,b) = (b,a)

instance toString ()
where
	toString _ = "()"

instance toString (a, b) | toString a & toString b
where
	toString (a, b) = concatArr5 "(" (toString a) ", " (toString b) ")"

instance toString (a, b, c) | toString a & toString b & toString c
where
	toString (a, b, c) = concatArr ["(", toString a, ", ", toString b, ", ", toString c, ")"]

instance toString (a, b, c, d) | toString a & toString b & toString c & toString d
where
	toString (a, b, c, d) = concatArr ["(", toString a, ", ", toString b, ", ", toString c, ", ", toString d, ")"]

instance toString (a, b, c, d, e) | toString a & toString b & toString c & toString d & toString e
where
	toString (a, b, c, d, e) =
		concatArr ["(", toString a, ", ", toString b, ", ", toString c, ", ", toString d, ", ", toString e, ")"]
