implementation module Data.Generics

import Data.Array
import StdGeneric
import StdEnv

class genericDescriptorName a :: !a -> String
instance genericDescriptorName GenericTypeDefDescriptor where genericDescriptorName gtd = gtd.gtd_name
instance genericDescriptorName GenericConsDescriptor where genericDescriptorName gcd = gcd.gcd_name
instance genericDescriptorName GenericRecordDescriptor where genericDescriptorName grd = grd.grd_name
instance genericDescriptorName GenericFieldDescriptor where genericDescriptorName gfd = gfd.gfd_name

class genericDescriptorType a :: !a -> GenType
instance genericDescriptorType GenericTypeDefDescriptor where
	genericDescriptorType gtd = foldl GenTypeApp (GenTypeCons gtd.gtd_name) [GenTypeVar i\\i<-[0..gtd.gtd_arity]]
instance genericDescriptorType GenericConsDescriptor where
	genericDescriptorType gcd = gcd.gcd_type
instance genericDescriptorType GenericRecordDescriptor where
	genericDescriptorType grd = resType grd.grd_type
instance genericDescriptorType GenericFieldDescriptor where
	genericDescriptorType gfd = getFieldType gfd.gfd_cons.grd_type gfd.gfd_index

resType :: !GenType -> GenType
resType (GenTypeArrow l r) = resType r
resType t = t

getFieldType :: !GenType !Int -> GenType
getFieldType (GenTypeArrow l r) 0 = l
getFieldType (GenTypeArrow l r) idx = getFieldType r (dec idx)

instance toString GenType where
	toString t = concatArr (printGenType tvs t [])

tvs :: [String]
tvs =: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v"
	, "w", "x", "y", "z", "aa", "ab", "ac", "ad", "ae", "af"]

printGenType :: ![String] !GenType ![String] -> [String]
printGenType tvs (GenTypeCons a) acc = [a:acc]
printGenType tvs (GenTypeVar i) acc = [tvs !! i:acc]
printGenType tvs (GenTypeApp l r) acc = ["(":printGenType tvs l [" ":printGenType tvs r [")":acc]]]
printGenType tvs (GenTypeArrow l r) acc = printGenType tvs l ["->":printGenType tvs r acc]

fromOBJECT :: !(OBJECT x) -> x
fromOBJECT (OBJECT x) = x

fromCONS :: !(CONS x) -> x
fromCONS (CONS x) = x

fromRECORD :: !(RECORD x) -> x
fromRECORD (RECORD x) = x

fromFIELD :: !(FIELD x) -> x
fromFIELD (FIELD x) = x

fromPAIRX :: !(PAIR x y) -> x
fromPAIRX (PAIR x _) = x

fromPAIRY :: !(PAIR x y) -> y
fromPAIRY (PAIR _ y) = y

object :: !x -> OBJECT x
object x = OBJECT x

field :: !x -> FIELD x
field x = FIELD x
