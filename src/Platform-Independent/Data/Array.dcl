definition module Data.Array

from StdArray import class Array
from StdEnv import class List
from StdOverloaded import class +++

mapArrSt :: !(.a -> .(*st -> *(.a, *st))) !*(arr .a) !*st -> *(!*(arr .a), !*st) | Array arr a

foldrArr :: !(a -> .(.b -> .b)) !.b !.(arr a) -> .b | Array arr a

/**
 * Folds over an array and reduces it from right to left in a strict manner.
 * The fold function is applied to the rightmost element in the array first.
 *
 * @param The fold function, the `Int` is the current index within the array.
 * @param The initial accumulator.
 * @param The array to fold over.
 * @result The accumulator after applying the fold function on all elements of the array.
 * @complexity Space: O(1), Time: O(n).
 */
foldrArrWithKey :: !(Int a -> .(.b -> .b)) !.b !.(arr a) -> .b | Array arr a

foldrUArr :: !(a -> .(.b -> .(*(arr a) -> *(.b, *(arr a))))) .b !*(arr a)
          -> *(.b, *(arr a)) | Array arr a

foldrUArrWithKey :: !(Int a -> .(.b -> .(*(arr a) -> *(.b, *(arr a))))) .b !*(arr a)
                 -> *(.b, *(arr a)) | Array arr a

foldlArr :: !(.b -> .(a -> .b)) !.b !.(arr a) -> .b | Array arr a

foldlArrWithKey :: !(Int .b -> .(a -> .b)) !.b !.(arr a) -> .b | Array arr a

reverseArr :: !.(arr a) -> .arr a | Array arr a

takeArr :: !Int !.(arr a) -> .arr a | Array arr a

mapArr :: !(a -> a) !(arr a) -> arr a | Array arr a

appendArr :: !.(arr1 a) !.(arr2 a) -> .(arr3 a) | Array arr1 a & Array arr2 a & Array arr3 a

instance +++ {a}
instance +++ {!a}

reduceArray :: ((.a -> u:(b -> b)) -> .(b -> .(c -> .a))) (.a -> u:(b -> b)) b !.(d c) -> b | Array d c

//* Concatenates a list of arrays in `O(n)`, where `n` is the total number of elements.
concatArr :: !(l (a e)) -> a e | Array a e & List l (a e) special
	l = [], a = {}; l = [], a = {!}; l = [], a = {#}, e = Int; l = [], a = {#}, e = Char;
	l = [], a = {#}, e = Bool; l = [], a = {#}, e = Real
	l = [!], a = {}; l = [!], a = {!}; l = [!], a = {#}, e = Int; l = [!], a = {#}, e = Char;
	l = [!], a = {#}, e = Bool; l = [!], a = {#}, e = Real
	l = [ !], a = {}; l = [ !], a = {!}; l = [ !], a = {#}, e = Int; l = [ !], a = {#}, e = Char;
	l = [ !], a = {#}, e = Bool; l = [ !], a = {#}, e = Real
	l = [!!], a = {}; l = [!!], a = {!}; l = [!!], a = {#}, e = Int; l = [!!], a = {#}, e = Char;
	l = [!!], a = {#}, e = Bool; l = [!!], a = {#}, e = Real

/**
 * Concatenates three arrays in `O(n)`, where `n` is the total number of elements, more efficiently than the general
 * {{`concatArr`}}.
 */
concatArr3 :: !(a e) !(a e) !(a e) -> (a e) | Array a e
	special a = {}; a = {!}; a = {#}, e = Int; a = {#}, e = Char; a = {#}, e = Bool; a = {#}, e = Real

/**
 * Concatenates three arrays in `O(n)`, where `n` is the total number of elements, more efficiently than the general
 * {{`concatArr`}}.
 */
concatArr4 :: !(a e) !(a e) !(a e) !(a e) -> (a e) | Array a e
	special a = {}; a = {!}; a = {#}, e = Int; a = {#}, e = Char; a = {#}, e = Bool; a = {#}, e = Real

/**
 * Concatenates three arrays in `O(n)`, where `n` is the total number of elements, more efficiently than the general
 * {{`concatArr`}}.
 */
concatArr5 :: !(a e) !(a e) !(a e) !(a e) !(a e) -> (a e) | Array a e
	special a = {}; a = {!}; a = {#}, e = Int; a = {#}, e = Char; a = {#}, e = Bool; a = {#}, e = Real
