definition module Data.Either
/**
* This module defines the "Either" type to represent binary choice.
* Clean's generics define a similar type EITHER, but this should only be
* used inside generic functions, since most generic functions treat this
* type in a special way which may lead to strange behavior.
*/
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenFDomain import generic gFDomain
from Data.GenLexOrd import generic gLexOrd, :: LexOrd
from StdOverloaded import class ==, class <

/**
 * The Either type represents a binary choice. It is often used to represent
 * correct (Right) or error (Left).
 */
:: Either a b = Left a | Right b

instance == (Either a b) | == a & == b

instance < (Either a b) | < a & < b

instance toString (Either a b) | toString a & toString b

derive gEq Either
derive gDefault Either
derive gFDomain Either
derive gLexOrd Either

/**
 * Apply a function to the {{Either}}
 *
 * @param function to apply on the value of the {{Left}}
 * @param function to apply on the value of the {{Right}}
 * @param the either value
 */
either    :: .(.a -> .c) .(.b -> .c) !(Either .a .b) -> .c

//* Extract the {{Left}}s from a list of {{Either}}s.
lefts     :: !.[Either .a .b] -> .[.a]

//* Extract the {{Right}}s from a list of {{Either}}s.
rights    :: !.[Either .a .b] -> .[.b]

//* Extract the {{Left}} value. If it is a {{Right}}, use the default value.
fromLeft  :: .a !(Either .a .b) -> .a

//* Extract the {{Right}} value. If it is a {{Left}}, use the default value.
fromRight :: .b !(Either .a .b) -> .b
