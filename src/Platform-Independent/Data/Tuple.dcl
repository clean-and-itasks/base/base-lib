definition module Data.Tuple

from StdOverloaded import class toString

tuple  :: .a .b -> .(.a,.b)
tuple3 :: .a .b .c -> .(.a,.b,.c)

appFst :: .(.a -> .c) !(.a,.b) -> (.c,.b)
appSnd :: .(.b -> .c) !(.a,.b) -> (.a,.c)

appFst3 :: .(.a -> .d) !(.a,.b,.c) -> (.d,.b,.c)
appSnd3 :: .(.b -> .d) !(.a,.b,.c) -> (.a,.d,.c)
appThd3 :: .(.c -> .d) !(.a,.b,.c) -> (.a,.b,.d)

swap :: !.(.a, .b) -> .(.b, .a)

instance toString ()
instance toString (a, b) | toString a & toString b
instance toString (a, b, c) | toString a & toString b & toString c
instance toString (a, b, c, d) | toString a & toString b & toString c & toString d
instance toString (a, b, c, d, e) | toString a & toString b & toString c & toString d & toString e
